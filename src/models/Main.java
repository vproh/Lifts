package models;

import builders.LiftBuilder;
import builders.UserBuilder;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class Main {
    private static Logger logger = Logger.getLogger("Logging Exception");

    private static void logException(Exception e) {
        StringWriter trace = new StringWriter();
        e.printStackTrace(new PrintWriter(trace));
        String msg = trace.toString();
        logger.severe(msg);
    }
    public static void main(String[] args) {
        List<Lift> lifts;
        User user;

        try {
            lifts = new ArrayList<>(Arrays.asList(
                    new LiftBuilder().setCurrentFloor(4).setFinishFloor(4).build(),
                    new LiftBuilder().setCurrentFloor(8).setFinishFloor(3).build(),
                    new LiftBuilder().setCurrentFloor(3).setFinishFloor(5).build(),
                    new LiftBuilder().setCurrentFloor(10).setFinishFloor(1).build(),
                    new LiftBuilder().setCurrentFloor(6).setFinishFloor(4).build()

            ));

            user =  new UserBuilder().setCurrentFloor(1).setUserDirection(Direction.STOPPED).build();
            System.out.println(user.getBestLift(lifts));

        } catch (IllegalArgumentException e) {
            logException(e);
        }
    }
}
