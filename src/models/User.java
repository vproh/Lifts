package models;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class User {
    private int currentFloor;
    private Direction userDirection;

    public void setUserDirection(Direction userDirection) {
        this.userDirection = userDirection;
    }

    public void setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;
    }

    private boolean isUserBetweenLiftStations(Lift lift) {
        if (lift.getCurrentFloor() > lift.getFinishFloor() &&
                lift.getCurrentFloor() >= currentFloor &&
                lift.getFinishFloor() <= currentFloor) {
            return true;
        }

        return lift.getCurrentFloor() < lift.getFinishFloor() &&
                lift.getCurrentFloor() <= currentFloor &&
                lift.getFinishFloor() >= currentFloor;

    }

    private List<Lift> getLiftsByDirection(List<Lift> lifts, Direction direction) {
        return lifts.stream()
                .filter(l -> l.getLiftDirection() == direction)
                .collect(Collectors.toList());
    }

    private List<Lift> getLiftsAcrossUserFloor(List<Lift> lifts) {
        return lifts.stream()
                .filter(this::isUserBetweenLiftStations)
                .collect(Collectors.toList());
    }

    private List<Lift> getLiftsFinishFloorSameWithUserFloor(List<Lift> lifts) {
        return lifts.stream()
                .filter(l -> l.getFinishFloor() == currentFloor && l.getLiftDirection() != Direction.STOPPED)
                .collect(Collectors.toList());
    }

    private List<Lift> getBreakDirectionLiftsWithSameUserFloor(List<Lift> lifts) {
        return lifts.stream()
                .filter(l -> l.getFinishFloor() == currentFloor && l.getLiftDirection() == Direction.STOPPED)
                .collect(Collectors.toList());
    }

    private Lift getLiftWithBestDistanceToUser(List<Lift> lifts) {
        return lifts.stream()
                .min(Comparator.comparing(l -> l.getCurrentDistanceToUser(currentFloor)))
                .orElse(null);
    }

    /* Lifts with the smallest distance to the user */
    private Lift getBestLiftOfOtherDirection(List<Lift> lifts) {
        return lifts.stream()
                .min(Comparator.comparing(l -> Math.abs(l.getDistanceBetweenFloors() + l.getFinishDistanceToUser(currentFloor))))
                .orElse(null);
    }

    public Lift getBestLift(List<Lift> lifts) {
        List<Lift> sameDirectionLifts = getLiftsByDirection(lifts, userDirection);
        List<Lift> liftsAcrossUserFloor = getLiftsAcrossUserFloor(sameDirectionLifts);

        if (!sameDirectionLifts.isEmpty() && !liftsAcrossUserFloor.isEmpty())
            return getLiftWithBestDistanceToUser(liftsAcrossUserFloor);

        List<Lift> liftsFinishFloorSameWithUserFloor = getLiftsFinishFloorSameWithUserFloor(lifts);
        if (!liftsFinishFloorSameWithUserFloor.isEmpty())
            return getLiftWithBestDistanceToUser(liftsFinishFloorSameWithUserFloor);

        List<Lift> breakDirectionLifts = getLiftsByDirection(lifts, Direction.STOPPED);
        if (!breakDirectionLifts.isEmpty())
            return getBreakDirectionLiftsWithSameUserFloor(breakDirectionLifts).get(0);

        return getBestLiftOfOtherDirection(lifts);
    }
}
