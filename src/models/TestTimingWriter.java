package models;

import org.testng.*;

import java.io.*;
import java.util.logging.Logger;

public class TestTimingWriter implements ITestListener {
    private static final String TIMING_REPORT_FILE_PATH = "timingReport.txt";
    private static final String TIME_FORMAT_MSC = "msc";
    private static final String TIME_FORMAT_NANO = "nanosec";
    private static final String TEST_DIVIDER = "\r\n********************************************************************************************\r\n";
    private static final String MSC_NANO_DIVIDER = " | ";


    private int testId = 0;

    private int summaryTimeMsc = 0;
    private long summaryTimeNano = 0;

    private long nanoTiming;

    private BufferedWriter bufferedWriter = null;
    private static Logger logger = Logger.getLogger("Logging Exception");

    private static void logException(Exception e) {
        StringWriter trace = new StringWriter();
        e.printStackTrace(new PrintWriter(trace));
        String msg = trace.toString();
        logger.severe(msg);
    }

    private void writeMessageToFile(String message) {
        try {
            bufferedWriter.append(message + "\r\n");
        } catch (IOException e) {
            logException(e);
        }
    }

    @Override
    public void onTestStart(ITestResult iTestResult) {
        nanoTiming = System.nanoTime();
        testId ++;
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        long resultTimingTest = iTestResult.getEndMillis() - iTestResult.getStartMillis();
        nanoTiming = System.nanoTime() - nanoTiming;

        summaryTimeMsc += resultTimingTest;
        summaryTimeNano += nanoTiming;

        String testTimingReportMsc = iTestResult.getName() + testId + " => Timing = " + resultTimingTest + TIME_FORMAT_MSC +
                MSC_NANO_DIVIDER + nanoTiming + TIME_FORMAT_NANO;

        writeMessageToFile(testTimingReportMsc);
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        /* currently not useful */
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        /* currently not useful */
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
        /* currently not useful */
    }

    @Override
    public void onStart(ITestContext iTestContext) {
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(TIMING_REPORT_FILE_PATH, true));
        } catch (IOException e) {
            logException(e);
        }

        writeMessageToFile(iTestContext.getStartDate().toString() + "\r\n");
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        writeMessageToFile("Summary test timing = " + summaryTimeMsc + TIME_FORMAT_MSC + MSC_NANO_DIVIDER + summaryTimeNano + TIME_FORMAT_NANO);
        writeMessageToFile(TEST_DIVIDER);

        if (bufferedWriter != null) {
            try {
                bufferedWriter.close();
            } catch (IOException e) {
                logException(e);
            }
        }
    }
}
