package models;

import interfaces.PropertyActions;

import java.io.*;
import java.util.Properties;
import java.util.logging.Logger;

public class LiftProperty implements PropertyActions {
    private static  final String PROPERTY_FILE_PATH = "conf.properties";
    private static final String MIN_FLOOR_KEY = "minFloor";
    private static final String MAX_FLOOR_KEY = "maxFloors";

    private static Logger logger = Logger.getLogger("Logging Exception");

    private static void logException(Exception e) {
        StringWriter trace = new StringWriter();
        e.printStackTrace(new PrintWriter(trace));
        String msg = trace.toString();
        logger.severe(msg);
    }

    private static Properties properties = new Properties();

    public int getMaxFloorNumber() {
        return Integer.parseInt(getPropertyByKey(MAX_FLOOR_KEY));
    }

    public int getMinFloorNumber() {
        return Integer.parseInt(getPropertyByKey(MIN_FLOOR_KEY));
    }

    @Override
    public String getPropertyByKey(String key) {

        try (InputStream fileInputStream = new FileInputStream(PROPERTY_FILE_PATH)) {
            properties.load(fileInputStream);
        } catch (IOException e) {
            logException(e);
        }

        return properties.getProperty(key);
    }

    @Override
    public void setProperty(String key, String value) {

        try (OutputStream fileOutputStream = new FileOutputStream(PROPERTY_FILE_PATH)) {
            properties.setProperty(key, value);
            properties.store(fileOutputStream, null);
        } catch (IOException e) {
            logException(e);
        }
    }
}
