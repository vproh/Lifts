package models;

public class Lift {
    private static int counter = 1;
    private int id = counter ++;
    private int currentFloor;
    private int finishFloor;
    private Direction liftDirection;

    public int getCurrentFloor() {
        return currentFloor;
    }

    public void setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;
    }

    public int getFinishFloor() {
        return finishFloor;
    }

    public void setFinishFloor(int finishFloor) {
        this.finishFloor = finishFloor;
    }

    public int getId() {
        return id;
    }

    public Direction getLiftDirection() {
        return this.liftDirection;
    }

    public void setLiftDirection(Direction direction) {
        this.liftDirection = direction;
    }

    public static void resetCounterId() {
        counter = 1;
    }

    @Override
    public String toString() {
        return "Lift #" + id + "\nCurrent floor: " + currentFloor + "\nFinish Floor: " + finishFloor + "\nStatus: " + getLiftDirection();
    }

    public int getCurrentDistanceToUser(int userFloor) {
        return Math.abs(userFloor - currentFloor);
    }

    public int getFinishDistanceToUser(int userFloor) {
        return Math.abs(finishFloor - userFloor);
    }

    public int getDistanceBetweenFloors() {
        return Math.abs(finishFloor - currentFloor);
    }
}
