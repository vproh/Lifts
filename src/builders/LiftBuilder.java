package builders;

import interfaces.Builder;
import models.Direction;
import models.Lift;
import models.LiftProperty;

public class LiftBuilder implements Builder {
    private static final String INCORRECT_CURRENT_FLOOR_NUMBER_MESSAGE = "One or several lifts have incorrect current floor number!\n";
    private static final String INCORRECT_FINISH_FLOOR_NUMBER_MESSAGE = "One or several lifts have incorrect finish floor number!\n";

    private int currentFloor;
    private int finishFloor;

    public LiftBuilder setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;

        return this;
    }

    public LiftBuilder setFinishFloor(int finishFloor) {
        this.finishFloor = finishFloor;

        return this;
    }

    private void setUpLiftDirection(Lift lift) {
        if (currentFloor == finishFloor)
            lift.setLiftDirection(Direction.STOPPED);
        else if (currentFloor > finishFloor)
            lift.setLiftDirection(Direction.DOWN);
        else
            lift.setLiftDirection(Direction.UP);
    }

    @Override
    public Lift build() {
        Lift lift = new Lift();
        StringBuilder errorMessage = new StringBuilder();
        int maxFloorNumber = new LiftProperty().getMaxFloorNumber();
        int minFloorNumber = new LiftProperty().getMinFloorNumber();

        if (currentFloor > maxFloorNumber || currentFloor < minFloorNumber)
            errorMessage.append(INCORRECT_CURRENT_FLOOR_NUMBER_MESSAGE);
        if (finishFloor > maxFloorNumber || finishFloor < minFloorNumber)
            errorMessage.append(INCORRECT_FINISH_FLOOR_NUMBER_MESSAGE);

        if (!(errorMessage.toString().equals("")))
            throw new IllegalArgumentException(errorMessage.toString());

        lift.setCurrentFloor(currentFloor);
        lift.setFinishFloor(finishFloor);
        setUpLiftDirection(lift);

        return lift;
    }
}