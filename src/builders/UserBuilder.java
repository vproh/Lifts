package builders;

import interfaces.Builder;
import models.Direction;
import models.LiftProperty;
import models.User;

public class UserBuilder implements Builder {
    private static final String INCORRECT_CURRENT_FLOOR_NUMBER_MESSAGE = "You have entered incorrect floor number!\n";
    private static final String INCORRECT_DIRECTION_DOWN_MESSAGE = "You`re already on the first floor!\n";
    private static final String INCORRECT_DIRECTION_UP_MESSAGE = "You`re already on the last floor!\n";


    private int currentFloor;
    private Direction userDirection;

    public UserBuilder() { /* Do nothing because we have set methods. And fields have primitive type */ }

    public UserBuilder setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;

        return this;
    }

    public UserBuilder setUserDirection(Direction userDirection) {
        this.userDirection = userDirection;

        return this;
    }

    @Override
    public User build() {
        User user = new User();
        StringBuilder errorMessage = new StringBuilder();

        int maxFloorNumber = new LiftProperty().getMaxFloorNumber();
        int minFloorNumber = new LiftProperty().getMinFloorNumber();

        if (currentFloor > maxFloorNumber || currentFloor < minFloorNumber)
            errorMessage.append(INCORRECT_CURRENT_FLOOR_NUMBER_MESSAGE);
        if (currentFloor == minFloorNumber && userDirection == Direction.DOWN)
            errorMessage.append(INCORRECT_DIRECTION_DOWN_MESSAGE);
        if (currentFloor == maxFloorNumber && userDirection == Direction.UP)
            errorMessage.append(INCORRECT_DIRECTION_UP_MESSAGE);

        if (!(errorMessage.toString().equals("")))
            throw new IllegalArgumentException(errorMessage.toString());

        user.setCurrentFloor(currentFloor);
        user.setUserDirection(userDirection);

        return user;
    }
}
