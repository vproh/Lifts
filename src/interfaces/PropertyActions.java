package interfaces;

public interface PropertyActions {
    String getPropertyByKey(String key);
    void setProperty(String key, String value);

}
