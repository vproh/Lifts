package interfaces;

public interface Builder<T> {
    T build ();
}
